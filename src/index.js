import { render } from "react-dom";
import "./index.css";
import * as React from "react";
import { SampleBase } from "./sample-base";
import {
  DocumentEditorContainerComponent,
  Toolbar,
} from "@syncfusion/ej2-react-documenteditor";
import { TitleBar } from "./title-bar";

import { DialogUtility, Dialog } from "@syncfusion/ej2-react-popups";
import { ListView } from "@syncfusion/ej2-react-lists";
DocumentEditorContainerComponent.Inject(Toolbar);
// tslint:disable:max-line-length
export class MailMerge extends SampleBase {
  constructor() {
    super(...arguments);
    // this.hostUrl = 'https://ej2services.syncfusion.com/production/web-services/';
    this.hostUrl = "http://localhost:62870/";
    this.toolbarOptions = [
      "New",
      "Open",
      "Separator",
      "Undo",
      "Redo",
      "Separator",
      {
        prefixIcon: "sf-icon-InsertMergeField",
        tooltipText: "Insert Field",
        text: this.onWrapText("Insert Field"),
        id: "InsertField",
      },
      {
        prefixIcon: "sf-icon-FinishMerge",
        tooltipText: "Merge Document",
        text: this.onWrapText("Merge Document"),
        id: "MergeDocument",
      },
      {
        prefixIcon: "export-pdf",
        tooltipText: "Export To PDF",
        text: this.onWrapText("Export To PDF"),
        id: "ExportPdf",
      },
      "Separator",
      "Image",
      "Table",
      "Hyperlink",
      "Bookmark",
      "TableOfContents",
      "Separator",
      "Header",
      "Footer",
      "PageSetup",
      "PageNumber",
      "Break",
      "Separator",
      "Find",
      "Separator",
      "Comments",
      "TrackChanges",
      "Separator",
      "LocalClipboard",
      "RestrictEditing",
      "Separator",
      "FormFields",
      "UpdateFields",
    ];
    this.insertFieldDialogObj = new Dialog({
      header: "Merge Field",
      content:
        '<div class="dialogContent">' +
        // tslint:disable-next-line:max-line-length
        '<label class="e-insert-field-label">Name:</label></br><input type="text" id="field_text" class="e-input" placeholder="Type a field to insert eg. FirstName">' +
        "</div>",
      showCloseIcon: true,
      isModal: true,
      width: "auto",
      height: "auto",
      close: this.closeFieldDialog,
      buttons: [
        {
          click: () => {
            let fieldNameTextBox = document.getElementById("field_text");
            let fieldName = fieldNameTextBox.value;
            if (fieldName !== "") {
              this.container.documentEditor.editor.insertField(
                "MERGEFIELD " + fieldName + " \\* MERGEFORMAT"
              );
            }
            this.insertFieldDialogObj.hide();
            this.container.documentEditor.focusIn();
          },
          buttonModel: {
            content: "Ok",
            cssClass: "e-flat",
            isPrimary: true,
          },
        },
        {
          click: () => {
            this.insertFieldDialogObj.hide();
            this.container.documentEditor.focusIn();
          },
          buttonModel: {
            content: "Cancel",
            cssClass: "e-flat",
          },
        },
      ],
    });
    this.onLoadDefault = () => {
      // this.container.documentEditor.open(JSON.stringify(defaultDocument));
      this.container.documentEditor.documentName = "Mail Merge";
      let item = this.toolbarOptions;
      this.container.toolbarItems = item;
      this.titleBar.updateDocumentTitle();
      this.container.documentChange = () => {
        this.titleBar.updateDocumentTitle();
        this.container.documentEditor.focusIn();
      };
      document
        .getElementById("listview")
        .addEventListener("dragstart", (event) => {
          event.dataTransfer.setData("Text", event.target.innerText);
          event.target.classList.add("de-drag-target");
        });
      // Prevent default drag over for document editor element
      document
        .getElementById("container")
        .addEventListener("dragover", (event) => {
          event.preventDefault();
        });
      // Drop Event for document editor element
      document.getElementById("container").addEventListener("drop", (e) => {
        let text = e.dataTransfer.getData("Text");
        this.container.documentEditor.selection.select({
          x: e.offsetX,
          y: e.offsetY,
          extend: false,
        });
        this.insertField(text);
      });
      document.addEventListener("dragend", (event) => {
        if (event.target.classList.contains("de-drag-target")) {
          event.target.classList.remove("de-drag-target");
        }
      });
      this.Data = [
        {
          text: "Description",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Photo1",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Photo2",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "TableStart:Detail",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "ScheduleId",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "CWPPlanStart",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "CWPPlanFinish",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "TableEnd:Detail",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "BeginGroup:ReferenceDocument",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Reference",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "EndGroup:ReferenceDocument",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "BeginGroup:Header",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "LogoLeft",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Project",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "CodeName",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Title",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Author",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Version",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "Date",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "LogoRight",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
        {
          text: "EndGroup:Header",
          category: "Drag or click the field to insert.",
          htmlAttributes: { draggable: true },
        },
      ];
      //this.field = {tooltip:'category',htmlAttributes: { draggable: true }};
      let listDivElement = document.getElementById("listview");
      let listView = new ListView({
        dataSource: this.Data,
        fields: { tooltip: "category" },
        select: onSelect.bind(this),
      });
      listView.appendTo(listDivElement);
      function onSelect(args) {
        let fieldName = args.text;
        const type = ["Photo1", "Photo2", "LogoLeft", "LogoRight"].includes(
          fieldName
        )
          ? "image"
          : "text";
        listView.selectItem(undefined);
        this.insertField(fieldName, type);
      }
      this.container.toolbarClick = (args) => {
        switch (args.item.id) {
          case "MergeDocument":
            this.mergeDocument();
            break;
          case "InsertField":
            this.showInsertFielddialog(this.container);
            break;
          case "ExportPdf":
            this.exportPdf();
        }
      };
    };
  }
  exportPdf() {
    this.container.documentEditor.saveAsBlob("Docx").then((blob) => {
      let exportedDocumment = blob;
      let fileReader = new FileReader();
      fileReader.onload = () => {
        let base64String = fileReader.result;
        let responseData = {
          fileName: this.container.documentEditor.documentName + ".docx",
          documentData: base64String,
        };

        this.showHideWaitingIndicator(true);
        let baseUrl = this.hostUrl + "api/wordprocessor/ExportPdf";
        let httpRequest = new XMLHttpRequest();
        httpRequest.open("POST", baseUrl, true);
        httpRequest.setRequestHeader(
          "Content-Type",
          "application/json;charset=UTF-8"
        );
        httpRequest.responseType = "blob";
        httpRequest.onreadystatechange = () => {
          if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200 || httpRequest.status === 304) {
              // this.container.documentEditor.open(httpRequest.responseText);
              console.log(
                "🚀 ~ file: index.js ~ line 315 ~ MailMerge ~ this.container.documentEditor.saveAsBlob ~ httpRequest.responseText",
                httpRequest.response
              );
              const url = window.URL.createObjectURL(
                new Blob([httpRequest.response], {
                  type: "application/pdf",
                })
              );
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", "preview.pdf");
              document.body.appendChild(link);
              link.click();
              link.parentNode.removeChild(link);
            } else {
              // Failed to export document
              DialogUtility.alert({
                title: "Information",
                content: "Failure to export document",
                showCloseIcon: true,
                closeOnEscape: true,
              });
            }
            this.showHideWaitingIndicator(false);
          }
        };
        httpRequest.send(JSON.stringify(responseData));
      };
      fileReader.readAsDataURL(blob);
    });
  }
  mergeDocument() {
    this.container.documentEditor.saveAsBlob("Docx").then((blob) => {
      let exportedDocumment = blob;
      let fileReader = new FileReader();
      fileReader.onload = () => {
        let base64String = fileReader.result;
        let responseData = {
          fileName: this.container.documentEditor.documentName + ".docx",
          documentData: base64String,
        };
        // let waitingPopUp:HTMLElement = document.getElementById('waiting-popup');
        // let inActiveDiv:HTMLElement = document.getElementById('popup-overlay');
        this.showHideWaitingIndicator(true);
        let baseUrl = this.hostUrl + "api/wordprocessor/MailMerge";
        let httpRequest = new XMLHttpRequest();
        httpRequest.open("POST", baseUrl, true);
        httpRequest.setRequestHeader(
          "Content-Type",
          "application/json;charset=UTF-8"
        );
        httpRequest.onreadystatechange = () => {
          if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200 || httpRequest.status === 304) {
              this.container.documentEditor.open(httpRequest.responseText);
            } else {
              // Failed to merge document
              DialogUtility.alert({
                title: "Information",
                content: "failure to merge document",
                showCloseIcon: true,
                closeOnEscape: true,
              });
            }
            this.showHideWaitingIndicator(false);
          }
        };
        httpRequest.send(JSON.stringify(responseData));
      };
      fileReader.readAsDataURL(blob);
    });
  }
  showHideWaitingIndicator(show) {
    let waitingPopUp = document.getElementById("waiting-popup");
    let inActiveDiv = document.getElementById("popup-overlay");
    inActiveDiv.style.display = show ? "block" : "none";
    waitingPopUp.style.display = show ? "block" : "none";
  }
  showInsertFielddialog(container) {
    let instance = this;
    if (
      document.getElementById("insert_merge_field") === null ||
      document.getElementById("insert_merge_field") === undefined
    ) {
      let fieldcontainer = document.createElement("div");
      fieldcontainer.id = "insert_merge_field";
      document.body.appendChild(fieldcontainer);
      this.insertFieldDialogObj.appendTo("#insert_merge_field");
      fieldcontainer.parentElement.style.position = "fixed";
      fieldcontainer.style.width = "auto";
      fieldcontainer.style.height = "auto";
    }
    this.insertFieldDialogObj.close = () => {
      container.documentEditor.focusIn();
    };
    this.insertFieldDialogObj.beforeOpen = () => {
      container.documentEditor.focusIn();
    };
    this.insertFieldDialogObj.show();
    let fieldNameTextBox = document.getElementById("field_text");
    fieldNameTextBox.value = "";
  }
  closeFieldDialog() {
    this.insertFieldDialogObj.hide();
    this.container.documentEditor.focusIn();
  }
  insertField(fieldName, type = "text") {
    let fileName = fieldName
      .replace(/\n/g, "")
      .replace(/\r/g, "")
      .replace(/\r\n/g, "");
    let fieldCode =
      "MERGEFIELD  " +
      `${type === "image" ? "Image:" : ""}${fieldName}` +
      "  \\* MERGEFORMAT ";
    this.container.documentEditor.editor.insertField(
      fieldCode,
      "«" + `${type === "image" ? "Image:" : ""}${fieldName}` + "»"
    );
    this.container.documentEditor.focusIn();
  }
  onWrapText(text) {
    let content = "";
    let index = text.lastIndexOf(" ");
    content = text.slice(0, index);
    text.slice(index);
    content += '<div class="e-de-text-wrap">' + text.slice(index) + "</div>";
    return content;
  }
  /*
    onSelect(args: SelectEventArgs) {
        let fieldName: any = args.text;
       //this.listview.selectItem(undefined);
        this.insertField(fieldName);
    } */
  rendereComplete() {
    this.container.serviceUrl = this.hostUrl + "api/documenteditor/";
    this.container.documentEditor.pageOutline = "#E0E0E0";
    this.container.documentEditor.acceptTab = true;
    this.container.documentEditor.resize();
    this.titleBar = new TitleBar(
      document.getElementById("documenteditor_titlebar"),
      this.container.documentEditor,
      true
    );
    this.onLoadDefault();
  }
  render() {
    return (
      <div className="control-pane">
        <div className="control-section">
          <div id="documenteditor_titlebar" className="e-de-ctn-title"></div>
          <div
            className="col-lg-2 control-section"
            style={{
              "padding-right": "inherit",
              paddingTop: "0px",
              "padding-left": "5px",
              height: "590px",
              "border-left": "1px solid rgb(238, 238, 238)",
              "border-bottom": "1px solid rgb(238, 238, 238)",
            }}
          >
            <h5>
              <label
                style={{
                  display: "block",
                  margin: "1px",
                  "padding-top": "5px",
                }}
              >
                Select Field to Insert
              </label>
            </h5>
            <div id="listview"></div>
          </div>
          <div
            className="col-lg-10 control-section"
            style={{
              "padding-left": "0px",
              paddingRight: "0px",
              "padding-top": "0px",
            }}
          >
            <DocumentEditorContainerComponent
              id="container"
              ref={(scope) => {
                this.container = scope;
              }}
              style={{ display: "block" }}
              height={"590px"}
              enableToolbar={true}
              locale="en-US"
            />
          </div>
        </div>

        <div className="overlay" id="popup-overlay"></div>
        <div id="waiting-popup">
          <svg className="circular" height="40" width="40">
            <circle
              className="circle-path"
              cx="25"
              cy="25"
              r="20"
              fill="none"
              stroke-width="6"
              stroke-miterlimit="10"
            />
          </svg>
        </div>
        <script>
          {
            (window.onbeforeunload = function () {
              return "Want to save your changes?";
            })
          }
        </script>
      </div>
    );
  }
}

render(<MailMerge />, document.getElementById("sample"));
